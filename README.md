# Thinventions programming task


This is the basis for a programming task on the FRDM-KL25Z development board. The code in this project uses the MMA8451Q accelerometer on the dev board to change the color of the RGB-LED dependent on which axis senses the maximum gravity (i.e. which side points downwards).

Your task is to use this code as a basis for the following application:

The LED should be green after a reset. Use the built-in 'Freefall detection' feature of the accelerometer to trigger an interrupt. Upon that interrupt the LED should turn red. After 5 seconds the LED should reset to green.

Stricly use the C99 standard (ISO/IEC 9899:1999).


## Installation instructions

- Download �NXP MCUXPresso� from https://goo.gl/17z3Tqo for your system - you have to create a free account with NXP - download just the IDE, no plugins or extensions
- Install the IDE and start it
- There may be additional software for the Segger J-Link debugger in the MCUXPresso package that you have to install
- Open 'File->Import' in the main menu
- Chose 'General->Existing Projects into Workspace'
- Click 'Next'
- Select this folder ('thinventions_task') as root directoy
- Click 'Finish'
- Connect the FRDM-KL25Z board via USB. Use the right side USB port (SDA).
- Click on the arrow next to the bug in the toolbar and chose 'Debug configurations'
- Open 'GDB PEMicro Interface Debugging' in the right panel and double click 'thinventions_task JLink Debug'
- The code will be compiled and flashed to the board.
- Change the orientation of the board to change the LED color
- You can now stop the code with the 'pause' icon in the toolbar and set breakpoints
- Find out the COM port the 'OpenSDA' device is connected to and open a serial console to it with 115200 baud - You should see debug output
- Stop the debugging with the 'stop' icon and switch back to the coding perspective with the icon on the left side of the toolbar.
- Familiarize yourself with the IDE and code away

## References

- MCUXpresso IDE user guide: https://www.nxp.com/docs/en/user-guide/MCUXpresso_IDE_User_Guide.pdf
- User guide for the FRDM-KL25Z board: https://www.mouser.com/pdfdocs/FRDM-KL25Z.pdf
- Datasheet for the MMA8451Q accelerometer: http://www.nxp.com/assets/documents/data/en/data-sheets/MMA8451Q.pdf


If you encounter severe problems you can't solve yourself that prevents you from finishing the task send a detailed description of the problems to rick@thinventions.de.
